-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.7.24 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             9.5.0.5332
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for ta
CREATE DATABASE IF NOT EXISTS `ta` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `ta`;

-- Dumping structure for table ta.anggaran
CREATE TABLE IF NOT EXISTS `anggaran` (
  `kd_ap` char(8) NOT NULL,
  `kd_produk` char(8) NOT NULL,
  `kd_produksi` char(8) NOT NULL,
  PRIMARY KEY (`kd_ap`),
  KEY `kd_produk` (`kd_produk`),
  KEY `kd_produksi` (`kd_produksi`),
  CONSTRAINT `anggaran_ibfk_1` FOREIGN KEY (`kd_produk`) REFERENCES `produk` (`kd_produk`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `anggaran_ibfk_2` FOREIGN KEY (`kd_produksi`) REFERENCES `produksi` (`kd_produksi`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table ta.anggaran: ~0 rows (approximately)
DELETE FROM `anggaran`;
/*!40000 ALTER TABLE `anggaran` DISABLE KEYS */;
INSERT INTO `anggaran` (`kd_ap`, `kd_produk`, `kd_produksi`) VALUES
	('AP01', 'P01', 'PS01');
/*!40000 ALTER TABLE `anggaran` ENABLE KEYS */;

-- Dumping structure for table ta.bahan_baku
CREATE TABLE IF NOT EXISTS `bahan_baku` (
  `kd_bb` char(8) NOT NULL,
  `nm_bb` char(25) NOT NULL,
  `satuan` char(10) NOT NULL,
  PRIMARY KEY (`kd_bb`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table ta.bahan_baku: ~3 rows (approximately)
DELETE FROM `bahan_baku`;
/*!40000 ALTER TABLE `bahan_baku` DISABLE KEYS */;
INSERT INTO `bahan_baku` (`kd_bb`, `nm_bb`, `satuan`) VALUES
	('B01', 'CABAI', 'Unit'),
	('B02', 'TOMAT', 'Kilogram'),
	('B03', 'GULA', 'Ons');
/*!40000 ALTER TABLE `bahan_baku` ENABLE KEYS */;

-- Dumping structure for table ta.detail_pemakaian
CREATE TABLE IF NOT EXISTS `detail_pemakaian` (
  `kd_detpemakaian` int(8) NOT NULL AUTO_INCREMENT,
  `kd_ap` char(8) NOT NULL,
  `kd_bb` char(8) NOT NULL,
  PRIMARY KEY (`kd_detpemakaian`),
  KEY `kd_ap` (`kd_ap`),
  KEY `kd_bb` (`kd_bb`),
  CONSTRAINT `detail_pemakaian_ibfk_1` FOREIGN KEY (`kd_ap`) REFERENCES `anggaran` (`kd_ap`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `detail_pemakaian_ibfk_2` FOREIGN KEY (`kd_bb`) REFERENCES `bahan_baku` (`kd_bb`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table ta.detail_pemakaian: ~0 rows (approximately)
DELETE FROM `detail_pemakaian`;
/*!40000 ALTER TABLE `detail_pemakaian` DISABLE KEYS */;
INSERT INTO `detail_pemakaian` (`kd_detpemakaian`, `kd_ap`, `kd_bb`) VALUES
	(1, 'AP01', 'B01');
/*!40000 ALTER TABLE `detail_pemakaian` ENABLE KEYS */;

-- Dumping structure for table ta.detail_produk
CREATE TABLE IF NOT EXISTS `detail_produk` (
  `kd_detproduk` int(8) NOT NULL AUTO_INCREMENT,
  `kd_produk` char(8) NOT NULL,
  `kd_bb` char(8) NOT NULL,
  PRIMARY KEY (`kd_detproduk`),
  KEY `kd_produk` (`kd_produk`),
  KEY `kd_bb` (`kd_bb`),
  CONSTRAINT `detail_produk_ibfk_1` FOREIGN KEY (`kd_produk`) REFERENCES `produk` (`kd_produk`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `detail_produk_ibfk_2` FOREIGN KEY (`kd_bb`) REFERENCES `bahan_baku` (`kd_bb`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

-- Dumping data for table ta.detail_produk: ~4 rows (approximately)
DELETE FROM `detail_produk`;
/*!40000 ALTER TABLE `detail_produk` DISABLE KEYS */;
INSERT INTO `detail_produk` (`kd_detproduk`, `kd_produk`, `kd_bb`) VALUES
	(10, 'P01', 'B01'),
	(11, 'P02', 'B02'),
	(12, 'P03', 'B03');
/*!40000 ALTER TABLE `detail_produk` ENABLE KEYS */;

-- Dumping structure for table ta.detail_produksi
CREATE TABLE IF NOT EXISTS `detail_produksi` (
  `kd_detproduksi` int(8) NOT NULL AUTO_INCREMENT,
  `kd_produksi` char(8) NOT NULL,
  `kd_produk` char(8) NOT NULL,
  `jml_produksi` int(11) NOT NULL,
  PRIMARY KEY (`kd_detproduksi`),
  KEY `kd_produksi` (`kd_produksi`),
  KEY `kd_produk` (`kd_produk`),
  CONSTRAINT `detail_produksi_ibfk_1` FOREIGN KEY (`kd_produksi`) REFERENCES `produksi` (`kd_produksi`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `detail_produksi_ibfk_2` FOREIGN KEY (`kd_produk`) REFERENCES `produk` (`kd_produk`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

-- Dumping data for table ta.detail_produksi: ~9 rows (approximately)
DELETE FROM `detail_produksi`;
/*!40000 ALTER TABLE `detail_produksi` DISABLE KEYS */;
INSERT INTO `detail_produksi` (`kd_detproduksi`, `kd_produksi`, `kd_produk`, `jml_produksi`) VALUES
	(9, 'PS01', 'P01', 32),
	(10, 'PS02', 'P02', 40),
	(11, 'PS03', 'P03', 20),
	(14, 'PS04', 'P01', 32),
	(16, 'PS05', 'P02', 44),
	(17, 'PS06', 'P03', 25),
	(18, 'PS07', 'P01', 36),
	(19, 'PS08', 'P02', 39),
	(20, 'PS09', 'P03', 20);
/*!40000 ALTER TABLE `detail_produksi` ENABLE KEYS */;

-- Dumping structure for table ta.detail_standar
CREATE TABLE IF NOT EXISTS `detail_standar` (
  `kd_detstandar` int(8) NOT NULL AUTO_INCREMENT,
  `kd_standar` char(8) NOT NULL,
  `kd_bb` char(8) NOT NULL,
  `unit_satuan` int(11) NOT NULL,
  `hrg_standar` int(11) NOT NULL,
  PRIMARY KEY (`kd_detstandar`),
  KEY `kd_standar` (`kd_standar`),
  KEY `kd_bb` (`kd_bb`),
  CONSTRAINT `detail_standar_ibfk_1` FOREIGN KEY (`kd_standar`) REFERENCES `standar` (`kd_standar`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `detail_standar_ibfk_2` FOREIGN KEY (`kd_bb`) REFERENCES `bahan_baku` (`kd_bb`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

-- Dumping data for table ta.detail_standar: ~9 rows (approximately)
DELETE FROM `detail_standar`;
/*!40000 ALTER TABLE `detail_standar` DISABLE KEYS */;
INSERT INTO `detail_standar` (`kd_detstandar`, `kd_standar`, `kd_bb`, `unit_satuan`, `hrg_standar`) VALUES
	(9, 'S01', 'B01', 2, 20000),
	(12, 'S02', 'B01', 3, 20000),
	(13, 'S03', 'B01', 2, 20000),
	(15, 'S04', 'B02', 3, 3000),
	(16, 'S05', 'B02', 2, 3000),
	(17, 'S06', 'B02', 1, 3000),
	(18, 'S07', 'B03', 10, 6000),
	(19, 'S08', 'B03', 7, 6000),
	(20, 'S09', 'B03', 3, 6000);
/*!40000 ALTER TABLE `detail_standar` ENABLE KEYS */;

-- Dumping structure for table ta.produk
CREATE TABLE IF NOT EXISTS `produk` (
  `kd_produk` char(8) NOT NULL,
  `nm_produk` char(25) NOT NULL,
  PRIMARY KEY (`kd_produk`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table ta.produk: ~4 rows (approximately)
DELETE FROM `produk`;
/*!40000 ALTER TABLE `produk` DISABLE KEYS */;
INSERT INTO `produk` (`kd_produk`, `nm_produk`) VALUES
	('P01', 'SAOS ESTRA PEDAS'),
	('P02', 'SAOS PEDAS'),
	('P03', 'SAOS MANIS');
/*!40000 ALTER TABLE `produk` ENABLE KEYS */;

-- Dumping structure for table ta.produksi
CREATE TABLE IF NOT EXISTS `produksi` (
  `kd_produksi` char(8) NOT NULL,
  `masa` int(11) NOT NULL,
  PRIMARY KEY (`kd_produksi`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table ta.produksi: ~9 rows (approximately)
DELETE FROM `produksi`;
/*!40000 ALTER TABLE `produksi` DISABLE KEYS */;
INSERT INTO `produksi` (`kd_produksi`, `masa`) VALUES
	('PS01', 1),
	('PS02', 1),
	('PS03', 1),
	('PS04', 2),
	('PS05', 2),
	('PS06', 2),
	('PS07', 3),
	('PS08', 3),
	('PS09', 3);
/*!40000 ALTER TABLE `produksi` ENABLE KEYS */;

-- Dumping structure for table ta.standar
CREATE TABLE IF NOT EXISTS `standar` (
  `kd_standar` char(8) NOT NULL,
  `kd_produk` char(8) NOT NULL,
  `jml_standar` int(11) NOT NULL,
  PRIMARY KEY (`kd_standar`),
  KEY `kd_produk` (`kd_produk`),
  CONSTRAINT `standar_ibfk_1` FOREIGN KEY (`kd_produk`) REFERENCES `produk` (`kd_produk`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table ta.standar: ~9 rows (approximately)
DELETE FROM `standar`;
/*!40000 ALTER TABLE `standar` DISABLE KEYS */;
INSERT INTO `standar` (`kd_standar`, `kd_produk`, `jml_standar`) VALUES
	('S01', 'P01', 40000),
	('S02', 'P02', 60000),
	('S03', 'P03', 40000),
	('S04', 'P01', 9000),
	('S05', 'P02', 6000),
	('S06', 'P03', 3000),
	('S07', 'P01', 60000),
	('S08', 'P02', 42000),
	('S09', 'P03', 18000);
/*!40000 ALTER TABLE `standar` ENABLE KEYS */;

-- Dumping structure for table ta.user
CREATE TABLE IF NOT EXISTS `user` (
  `kd_user` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `admin` enum('Y','N') NOT NULL,
  `username` varchar(15) NOT NULL,
  `password` varchar(50) NOT NULL,
  `jk` varchar(15) DEFAULT NULL,
  `no_hp` varchar(15) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `alamat` text,
  PRIMARY KEY (`kd_user`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Dumping data for table ta.user: ~2 rows (approximately)
DELETE FROM `user`;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`kd_user`, `nama`, `admin`, `username`, `password`, `jk`, `no_hp`, `email`, `alamat`) VALUES
	(1, 'Ichsan Munadi', 'Y', 'papiyot', '5f4dcc3b5aa765d61d8327deb882cf99', 'Laki-laki', '82325031088', 'ichsan.dgn@gmail.com', 'Solo'),
	(2, 'coba', 'Y', 'coba', '5f4dcc3b5aa765d61d8327deb882cf99', 'Perempuan', '', 'admin@email.com', '');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
