<?php

clearstatcache();
include "koneksi.php";

if ($_GET['set'] == "bahan_baku") {
    include "module/bahan_baku/bahan_baku.php";
}
elseif($_GET['set'] == "masa_produksi"){
    include "module/masa_produksi/masa_produksi.php";
}
elseif($_GET['set'] == "produk"){
    include "module/produk/produk.php";
}
elseif($_GET['set'] == "detail_produk"){
    include "module/detail_produk/detail_produk.php";
}
elseif($_GET['set'] == "standar"){
    include "module/standar/standar.php";
}
elseif($_GET['set'] == "produksi"){
    include "module/produksi/produksi.php";
}
elseif($_GET['set'] == "lap_standar"){
    include "module/lap_standar/lap_standar.php";
}
elseif($_GET['set'] == "lap_bahanbaku"){
    include "module/lap_bahanbaku/lap_bahanbaku.php";
}
elseif($_GET['set'] == "lap_anggaran"){
    include "module/lap_anggaran/lap_anggaran.php";
}
else{
    echo"Modul Belum Dibuat.";
}

?>