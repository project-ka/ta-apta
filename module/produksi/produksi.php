<?php
$aksi="module/produksi/aksi.php";
?>
<div >
    <h2> Data Produksi</h2>
    <button type="button" class="btn btn-info" onclick="setdata('add')" >TAMBAH</button>
    <table class="table table-striped table-bordered table-responsive table-dark table-hover">
        <thead>
        <tr>
            <th scope="col" rowspan="2" style="vertical-align: middle; text-align: center;">No</th>
            <th scope="col" rowspan="2" style="vertical-align: middle; text-align: center;">Produk</th>
            <th scope="col" colspan="4" style="vertical-align: middle; text-align: center;">Jumlah Produksi Dalam</th>
        </tr>
        <tr>
            <th scope="col" style="vertical-align: middle; text-align: center;">Masa 1</th>
            <th scope="col" style="vertical-align: middle; text-align: center;">Masa 2</th>
            <th scope="col" style="vertical-align: middle; text-align: center;">Masa 3</th>
            <th scope="col" style="vertical-align: middle; text-align: center;">Masa 4</th>
        </tr>
        </thead>
        <tbody>
            <?php
            $no=1;
            $sql_l="SELECT * FROM produksi,produk where produksi.kd_produk=produk.kd_produk  ORDER BY kd_produksi";
            $hasil = $con->query($sql_l);
            $ketemuu=mysqli_num_rows($hasil);
            if ($ketemuu>0){
            while($set=$hasil->fetch_assoc()) {
                ?>
            <tr>
                <td style="vertical-align: middle; text-align: center;"><?php echo $no; ?></td>
                <td style="vertical-align: middle; text-align: center;"><?php echo $set['nm_produk']; ?></td>
                <td style="vertical-align: middle; text-align: center;"><?php echo $set['masa_1']; ?></td>
                <td style="vertical-align: middle; text-align: center;"><?php echo $set['masa_2']; ?></td>
                <td style="vertical-align: middle; text-align: center;"><?php echo $set['masa_3']; ?></td>
                <td style="vertical-align: middle; text-align: center;"><?php echo $set['masa_4']; ?></td>
            </tr>

                <?php
                $no++;  }
            }else{
                ?>
                <tr>
                    <td colspan="3">Data Kosong</td>
                </tr>
                <?php
            }
            ?>
        </tbody>
    </table>
    <div class="modal fade" id="ModalForm" tabindex="-1" role="dialog" aria-labelledby="ModalFormLabel1">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="ModalFormLabel1">Tambah Produksi</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <form method="post" action="<?php echo $aksi; ?>?set=produksi&aksi=tambah" >
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Produk:</label>
                            <select class="form-control" required name="produk" id="produk">
                                <?php
                                $sql_la="SELECT * FROM produk ORDER BY kd_produk";
                                $hasila = $con->query($sql_la);
                                while($get=$hasila->fetch_assoc()) {
                                    ?>
                                    <option value="<?=$get['kd_produk'];?>"><?=$get['nm_produk'];?></option>

                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Masa:</label>
                            <select class="form-control" required name="masa" id="masa">
                                <option value="1">Masa 1</option>
                                <option value="2">Masa 2</option>
                                <option value="3">Masa 3</option>
                                <option value="4">Masa 4</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Jumlah:</label>
                            <input type="number" class="form-control" required name="jumlah" id="jumlah">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>
<script>
    function setdata(type, id=null, name=null, status=null) {
        $('#ModalForm form')[0].reset();
        if (type=='add'){
            $('#ModalForm').modal('show');
        }
    }
</script>