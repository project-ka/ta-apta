<?php
$aksi="module/detail_produk/aksi.php";
$sql ="SELECT * from produk where kd_produk= '".$_GET["kode"]."'";
$hasil = $con->query($sql);
$data = $hasil->fetch_assoc();
?>
<div >
    <h2> Standar Produk <?=$data['nm_produk'];?> Per <?=$data['satuan_produk'];?></h2>
    <button type="button" class="btn btn-info" onclick="setdata('add')" >TAMBAH</button>
    <table class="table table-striped table-bordered table-responsive table-dark table-hover">
        <thead>
        <tr>
            <th scope="col">No</th>
            <th scope="col">Kode</th>
            <th scope="col">Bahan Baku</th>
        </tr>
        </thead>
        <tbody>
            <?php
            $no=1;
            $sql_l="SELECT * FROM detail_produk,bahan_baku where detail_produk.kd_bb=bahan_baku.kd_bb and detail_produk.kd_produk= '".$_GET["kode"]."' ORDER BY  detail_produk.kd_bb";
            $hasil = $con->query($sql_l);
            $ketemuu=mysqli_num_rows($hasil);
            if ($ketemuu>0){
                while($set=$hasil->fetch_assoc()) {
                    ?>
                    <tr>
                        <td><?php echo $no; ?></td>
                        <td><?php echo $set['kd_bb']; ?></td>
                        <td><?php echo $set['nm_bb']; ?></td>
                    </tr>

                    <?php
                    $no++;  }
            }else{
                ?>
                <tr>
                    <td colspan="3">Data Kosong</td>
                </tr>
            <?php
            }

            ?>
        </tbody>
    </table>
    <div class="modal fade" id="ModalForm" tabindex="-1" role="dialog" aria-labelledby="ModalFormLabel1">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="ModalFormLabel1">Tambah Bahan Baku</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <form method="post" action="<?php echo $aksi; ?>?set=detail_produk&aksi=tambah" >
                    <div class="modal-body">
                        <input type="hidden" id="kd_produk" name="kd_produk" value="<?=$_GET['kode'];?>">
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Bahan baku:</label>
                            <select class="form-control" required name="bb" id="bb">
                                <?php
                                $sql_la="SELECT * FROM bahan_baku ORDER BY kd_bb";
                                $hasila = $con->query($sql_la);
                                while($get=$hasila->fetch_assoc()) {
                                    ?>
                                    <option value="<?=$get['kd_bb'];?>"><?=$get['nm_bb'];?></option>

                                    <?php
                                }
                                ?>
                            </select>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>
<script>
    function setdata(type, id=null, name=null, status=null) {
        $('#ModalForm form')[0].reset();
        if (type=='add'){
            $('#ModalForm').modal('show');
        }
    }
</script>