<?php
$aksi="module/produk/aksi.php";
?>
<div >
    <h2> Data Produk</h2>
    <button type="button" class="btn btn-info" onclick="setdata('add')" >TAMBAH</button>
    <table class="table table-striped table-bordered table-responsive table-dark table-hover">
        <thead>
        <tr>
            <th scope="col">No</th>
            <th scope="col">Kode</th>
            <th scope="col">Produk</th>
            <th scope="col">Satuan</th>
            <th scope="col">Aksi</th>
        </tr>
        </thead>
        <tbody>
            <?php
            $no=1;
            $sql_l="SELECT * FROM produk ORDER BY kd_produk";
            $hasil = $con->query($sql_l);
            while($set=$hasil->fetch_assoc()) {
                ?>
            <tr>
                <td><?php echo $no; ?></td>
                <td><?php echo $set['kd_produk']; ?></td>
                <td><?php echo $set['nm_produk']; ?></td>
                <td><?php echo $set['satuan_produk']; ?></td>
                <td>
                    <a  class="btn btn-warning" href="?set=detail_produk&kode=<?php echo $set['kd_produk']; ?>" >Detail</a>
                    <a  class="btn btn-info" href="?set=standar&kode=<?php echo $set['kd_produk']; ?>" >Standar</a>
                </td>
            </tr>

        <?php
                $no++;  }
            ?>
        </tbody>
    </table>
    <div class="modal fade" id="ModalForm" tabindex="-1" role="dialog" aria-labelledby="ModalFormLabel1">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="ModalFormLabel1">Tambah Produk</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <form method="post" action="<?php echo $aksi; ?>?set=produk&aksi=tambah" >
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Produk:</label>
                            <input type="text" class="form-control" required name="produk" id="produk">
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Satuan:</label>
                            <select class="form-control" required name="satuan" id="satuan">
                                <option value="Kilogram">Kilogram</option>
                                <option value="Liter">Liter</option>
                                <option value="Unit">Unit</option>
                                <option value="Unit">Botol</option>
                                <option value="Unit">Sachet</option>
                                <option value="Unit">Porsi</option>
                            </select>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>
<script>
    function setdata(type, id=null, name=null, status=null) {
        $('#ModalForm form')[0].reset();
        if (type=='add'){
            $('#ModalForm').modal('show');
        }
    }
</script>