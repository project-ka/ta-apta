<?php
$aksi="module/standar/aksi.php";
$sql ="SELECT * from produk where kd_produk= '".$_GET["kode"]."'";
$hasil = $con->query($sql);
$data = $hasil->fetch_assoc();
?>
<div >
    <h2> Standar Produk <?=$data['nm_produk'];?> Per <?=$data['satuan_produk'];?></h2>
    <button type="button" class="btn btn-info" onclick="setdata('add')" >Ubah</button>
    <table class="table table-striped table-bordered table-responsive table-dark table-hover">
        <thead>
        <tr>
            <th scope="col">No</th>
            <th scope="col">Kode</th>
            <th scope="col">Bahan Baku</th>
            <th scope="col">Jumlah</th>
            <th style="text-align: right" scope="col">Harga</th>
            <th style="text-align: right" scope="col">Total</th>
        </tr>
        </thead>
        <tbody>
            <?php
            $no=1;
            $sql_l="SELECT * FROM detail_produk,bahan_baku where detail_produk.kd_bb=bahan_baku.kd_bb and detail_produk.kd_produk= '".$_GET["kode"]."' ORDER BY  detail_produk.kd_bb";
            $hasil = $con->query($sql_l);
            $ketemuu=mysqli_num_rows($hasil);
            if ($ketemuu>0){
                while($set=$hasil->fetch_assoc()) {
                    ?>
                    <tr>
                        <td><?php echo $no; ?></td>
                        <td><?php echo $set['kd_bb']; ?></td>
                        <td><?php echo $set['nm_bb']; ?></td>
                        <td><?php echo format_angka($set['jml_standar']); ?></td>
                        <td style="text-align: right">Rp. <?php echo format_angka($set['harga']); ?></td>
                        <td style="text-align: right">Rp. <?php echo format_angka($set['jumlah']); ?></td>
                    </tr>

                    <?php
                    $no++;  }
            }else{
                ?>
                <tr>
                    <td colspan="3">Data Kosong</td>
                </tr>
            <?php
            }

            ?>
        </tbody>
    </table>
    <div class="modal fade" id="ModalForm" tabindex="-1" role="dialog" aria-labelledby="ModalFormLabel1">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="ModalFormLabel1">Standar Bahan Baku</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <form method="post" action="<?php echo $aksi; ?>?set=standar&aksi=tambah" >
                    <div class="modal-body">
                        <input type="hidden" id="kd_produk" name="kd_produk" value="<?=$_GET['kode'];?>">
                        <div class="col-12 row">
                            <div class="form-group col-4">
                                <label for="recipient-name" class="control-label">Bahan Baku:</label>
                            </div>
                            <div class="form-group col-4">
                                <label for="recipient-name" class="control-label">Jumlah:</label>
                            </div>
                            <div class="form-group col-4">
                                <label for="recipient-name" class="control-label">Harga:</label>
                            </div>
                        </div>
                        <?php
                        $no=1;
                        $sql_l="SELECT * FROM detail_produk,bahan_baku where detail_produk.kd_bb=bahan_baku.kd_bb and detail_produk.kd_produk= '".$_GET["kode"]."' ORDER BY  detail_produk.kd_bb";
                        $hasil = $con->query($sql_l);
                        $ketemuu=mysqli_num_rows($hasil);

                        while($set=$hasil->fetch_assoc()) {
                        ?>

                        <div class="col-12 row">
                            <div class="form-group col-4">
                                <input type="hidden" class="form-control" value="<?=$set['kd_bb'];?>" readonly required name="kdbb[]" id="kdbb">
                                <input type="text" class="form-control" value="<?=$set['nm_bb'];?>" readonly required name="bb[]" id="bb">

                            </div>
                            <div class="form-group col-4">
                                <input type="text" class="form-control" value="<?=$set['jml_standar'];?>" placeholder="jumlah" name="jumlah[]" id="jumlah">

                            </div>
                            <div class="form-group col-4">
                                <input type="number" class="form-control" value="<?=$set['harga'];?>" placeholder="harga" name="harga[]" id="harga">

                            </div>
                        </div>
                            <?php
                            $no++;  }

                        ?>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>
<script>
    function setdata(type, id=null, name=null, status=null) {
        $('#ModalForm form')[0].reset();
        if (type=='add'){
            $('#ModalForm').modal('show');
        }
    }
</script>