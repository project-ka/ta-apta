<?php
$aksi="module/masa_produksi/aksi.php";
?>
<div >
    <h2> Data Masa Produksi</h2>
    <button type="button" class="btn btn-info" onclick="setdata('add')" >TAMBAH</button>
    <table class="table table-striped table-bordered table-responsive table-dark table-hover">
        <thead>
        <tr>
            <th scope="col">No</th>
            <th scope="col">Kode</th>
            <th scope="col">Masa</th>
        </tr>
        </thead>
        <tbody>
            <?php
            $no=1;
            $sql_l="SELECT * FROM masa_produksi ORDER BY kd_masa";
            $hasil = $con->query($sql_l);
            while($set=$hasil->fetch_assoc()) {
                ?>
            <tr>
                <td><?php echo $no; ?></td>
                <td><?php echo $set['kd_masa']; ?></td>
                <td><?php echo $set['masa']; ?></td>
            </tr>

        <?php
                $no++;  }
            ?>
        </tbody>
    </table>
    <div class="modal fade" id="ModalForm" tabindex="-1" role="dialog" aria-labelledby="ModalFormLabel1">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="ModalFormLabel1">Tambah Masa Produksi</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <form method="post" action="<?php echo $aksi; ?>?set=masa_produksi&aksi=tambah" >
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Masa:</label>
                            <input type="text" class="form-control" required name="masa" id="masa">
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>
<script>
    function setdata(type, id=null, name=null, status=null) {
        $('#ModalForm form')[0].reset();
        if (type=='add'){
            $('#ModalForm').modal('show');
        }
    }
</script>