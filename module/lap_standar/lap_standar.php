<?php
$aksi="module/bahan_baku/aksi.php";
?>
<div class="col-lg-6">
    <form method="get" action="index.php?set=lap_standar&detail=PK01">
    <div class="input-group">
        <input type="hidden" name="set" value="lap_standar" class="form-control" aria-label="...">
        <select class="form-control" required name="detail" id="detail" placeholder="Cari Produk">
            <option value="">Cari Produk..</option>
            <?php
            $sql_la="SELECT * FROM produk ORDER BY kd_produk";
            $hasila = $con->query($sql_la);
            while($get=$hasila->fetch_assoc()) {
                ?>
                <option value="<?=$get['kd_produk'];?>"><?=strtoupper($get['nm_produk']);?></option>

                <?php
            }
            ?>
        </select>
        <span class="input-group-btn">
        <button class="btn btn-default" type="submit">Cari</button>
        </form>

    </div><!-- /input-group -->
</span>
</div><!-- /.col-lg-6 -->
<hr>
<?php
if (empty($_GET['detail'])){
    ?>
<!--    <div >-->
<!--        <h2> TAMPIL DATA STANDAR</h2>-->
<!--        <table class="table table-striped table-bordered table-responsive table-dark table-hover">-->
<!--            <thead>-->
<!--            <tr>-->
<!--                <th scope="col">PRODUK</th>-->
<!--                <th scope="col">BIAYA</th>-->
<!--            </tr>-->
<!--            </thead>-->
<!--            <tbody>-->
<!--            --><?php
//            $no=0;
//            $sql_l="SELECT  produk.kd_produk,  produk.nm_produk, IFNULL(s.storerest, 0) AS storerest FROM produk  LEFT JOIN ( SELECT kd_produk, SUM(jumlah) AS storerest FROM detail_produk GROUP BY detail_produk.kd_produk  ) s ON (produk.kd_produk = s.kd_produk) ";
//            $hasil = $con->query($sql_l);
//            while($set=$hasil->fetch_assoc()) {
//                $no=$no+$set['storerest'];
//                ?>
<!--                <tr>-->
<!--                    <td ><a style="color:#1b1e21" href="?set=lap_standar&detail=--><?//=$set['kd_produk']; ?><!--">--><?php //echo strtoupper($set['nm_produk']); ?><!--</a></td>-->
<!--                    <td><a style="color:#1b1e21" href="?set=lap_standar&detail=--><?//=$set['kd_produk']; ?><!--">--><?php //echo format_angka($set['storerest']); ?><!--</a></td>-->
<!--                </tr>-->
<!---->
<!--                --><?php // }
//            ?>
<!--            <tr>-->
<!--                <td ><strong>TOTAL </strong></td>-->
<!--                <td >--><?//=format_angka($no);?><!-- </td>-->
<!--            </tr>-->
<!--            </tbody>-->
<!--        </table>-->
<!---->
<!---->
<!--    </div>-->
<?php
}
else{
    $sql ="SELECT * from produk where kd_produk = '".$_GET["detail"]."'";
    $hasil = $con->query($sql);
    $data = $hasil->fetch_assoc();
    $nama = $data['nm_produk'];
    $satuan = $data['satuan_produk'];
    ?>
    <div >
        <h2> DETAIL STANDAR <?= strtoupper($nama);?> PER <?= strtoupper($satuan);?></h2>
        <table class="table table-striped table-bordered table-responsive table-dark table-hover">
            <thead>
            <tr>
                <th scope="col">BAHAN BAKU</th>
                <th scope="col">UNIT SATUAN</th>
                <th scope="col">SATUAN</th>
                <th style="text-align: right" scope="col">HARGA STANDAR</th>
                <th style="text-align: right" scope="col">JUMLAH</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $no=0;
            $sql_l="SELECT * from detail_produk, bahan_baku where detail_produk.kd_bb=bahan_baku.kd_bb and kd_produk = '".$_GET["detail"]."'";
            $hasil = $con->query($sql_l);
            while($set=$hasil->fetch_assoc()) {
                $no=$no+$set['jumlah'];
                ?>
                <tr>
                    <td ><?php echo strtoupper($set['nm_bb']); ?></td>
                    <td><?php echo $set['jml_standar']; ?></td>
                    <td><?php echo strtoupper($set['satuan']); ?></td>
                    <td style="text-align: right">Rp. <?php echo format_angka($set['harga']); ?></td>
                    <td style="text-align: right">Rp. <?php echo format_angka($set['jumlah']); ?></td>
                </tr>

                <?php  }
            ?>
            <tr>
                <td colspan="4"><strong>TOTAL </strong> </td>
                <td style="text-align: right">Rp. <?=format_angka($no);?> </td>
            </tr>
            </tbody>
        </table>


    </div>
<?php
}
?>

