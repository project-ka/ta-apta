<?php
$aksi="module/bahan_baku/aksi.php";
?>
<div >
    <h2> Data Bahan Baku</h2>
    <button type="button" class="btn btn-info" onclick="setdata('add')" >TAMBAH</button>
    <table class="table table-striped table-bordered table-responsive table-dark table-hover">
        <thead>
        <tr>
            <th scope="col">No</th>
            <th scope="col">Kode</th>
            <th scope="col">Nama</th>
            <th scope="col">Satuan</th>
        </tr>
        </thead>
        <tbody>
            <?php
            $no=1;
            $sql_l="SELECT * FROM bahan_baku ORDER BY kd_bb";
            $hasil = $con->query($sql_l);
            while($set=$hasil->fetch_assoc()) {
                ?>
            <tr>
                <td><?php echo $no; ?></td>
                <td><?php echo $set['kd_bb']; ?></td>
                <td><?php echo $set['nm_bb']; ?></td>
                <td><?php echo $set['satuan']; ?></td>
            </tr>

        <?php
                $no++;  }
            ?>
        </tbody>
    </table>
    <div class="modal fade" id="ModalForm" tabindex="-1" role="dialog" aria-labelledby="ModalFormLabel1">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="ModalFormLabel1">Tambah Bahan Baku</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <form method="post" action="<?php echo $aksi; ?>?set=bahan_baku&aksi=tambah" >
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Nama Bahan Baku:</label>
                            <input type="text" class="form-control" required name="nm_bb" id="nm_bb">
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Satuan:</label>
                            <select class="form-control" required name="satuan" id="satuan">
                                <option value="Kilogram">Kilogram</option>
                                <option value="Liter">Liter</option>
                                <option value="Unit">Unit</option>
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>
<script>
    function setdata(type, id=null, name=null, status=null) {
        $('#ModalForm form')[0].reset();
        if (type=='add'){
            $('#ModalForm').modal('show');
        }
    }
</script>