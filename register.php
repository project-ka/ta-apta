<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css" >
	<link rel="stylesheet" type="text/css" href="admin.css">
	<link rel="stylesheet" type="text/css" href="fontawesome-free/css/all.min.css">
    <title>APLIKASI TA </title>
      <style>
          body {
              margin: 0;
              padding: 0;
              background-color: #17a2b8;
              height: 100vh;
          }
          #login .container #login-row #login-column #login-box {
              margin-top: 120px;
              max-width: 600px;
              border: 1px solid #9C9C9C;
              background-color: #EAEAEA;
          }
          #login .container #login-row #login-column #login-box #login-form {
              padding: 20px;
          }
          #login .container #login-row #login-column #login-box #login-form #register-link {
              margin-top: -85px;
          }
      </style>
  </head>
  <body>
  <div id="login">
      <h3 class="text-center text-white pt-5">Register form</h3>
      <div class="container">
          <div id="login-row" class="row justify-content-center align-items-center">
              <div id="login-column" class="col-md-6">
                  <div id="login-box" class="col-md-12">
                      <form id="login-form" class="form" action="inc/reg.php" method="post">
                          <h3 class="text-center text-info">Register</h3>
                          <div class="form-group">
                              <label for="username" class="text-info">Username:</label><br>
                              <input type="text" required name="username" id="username" class="form-control">
                          </div>
                          <div class="form-group">
                              <label for="password" class="text-info">Password:</label><br>
                              <input type="password" required name="password" id="password" class="form-control">
                          </div>
                          <div class="form-group">
                              <label for="nama" class="text-info">Name:</label><br>
                              <input type="text" required name="nama" id="nama" class="form-control">
                          </div>
                          <div class="form-group">
                              <label for="jk" class="text-info">Genre:</label><br>
                              <select type="text" required name="jk" id="jk" class="form-control">
                                  <option value="">====Pilih Dara====</option>
                                  <option value="Laki-laki">Laki-laki</option>
                                  <option value="Perempuan">Perempuan</option>
                              </select>
                          </div>
                          <div class="form-group">
                              <label for="no_hp" class="text-info">Telepon:</label><br>
                              <input type="text"  name="no_hp" id="no_hp" class="form-control">
                          </div>
                          <div class="form-group">
                              <label for="email" class="text-info">Email:</label><br>
                              <input type="text" required name="email" id="email" class="form-control">
                          </div>
                          <div class="form-group">
                              <label for="alamat" class="text-info">Alamat:</label><br>
                              <textarea type="text"  name="alamat" id="alamat" class="form-control"></textarea>
                          </div>
                          <div class="form-group">
                              <br>
                              <input type="submit" name="submit" class="btn btn-info btn-md" value="submit">
                          </div>

                      </form>
                  </div>
              </div>
          </div>
      </div>
  </div>
  </body>
</html>