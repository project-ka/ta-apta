<!doctype html>
<?php
clearstatcache();
include "inc/cek_session.php";
include "inc/fungsi_hdt.php";
include "inc/inc.library.php";
include "koneksi.php";
if ($_SESSION[login]==0 ) {
    echo '<script>
	alert(\'Anda Menyalahi Hak AKSES!\');
	window.location="/inc/logout.php";
	</script>	';
}
?>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css" >
	<link rel="stylesheet" type="text/css" href="style.css">
	<link rel="stylesheet" type="text/css" href="fontawesome-free/css/all.min.css">
    <title>APLIKASI TA </title>
  </head>
  <body>

  <nav class="navbar navbar-expand-lg navbar-light bg-danger fixed-top">
      <a class="navbar-brand" href="#">ANGGARAN PEMAKAIAN BAHAN BAKU</a>
      <form class="form-inline my-2 my-lg-0 ml-auto">
          <a class="btn btn-info my-2 my-sm-0" href="inc/logout.php">Logout</a>
      </form>
  </nav>
  <div id="wrapper">

      <!-- Sidebar -->
      <div id="sidebar-wrapper">
          <ul class="sidebar-nav">
              <br>
              <br>
              <br>
              <?php if ($_SESSION['admin']=='Y'){
                  ?>


              <li>
                  <a href="?set=produk">Produk</a>
              </li>
              <li >
                  <a href="?set=bahan_baku">Bahan Baku</a>
              </li>
<!--              <li>-->
<!--                  <a href="?set=masa_produksi">Masa Produksi</a>-->
<!--              </li>-->
              <li>
                  <a href="?set=produksi">Produksi</a>
              </li>
                  <?php
              }?>
              <li>
                  <a href="?set=lap_standar">Laporan Standar</a>
              </li>
              <li>
                  <a href="?set=lap_bahanbaku">Laporan Bahan Baku</a>
              </li>
              <li>
                  <a href="?set=lap_anggaran">Laporan Anggaran</a>
              </li>
          </ul>
      </div>
      <!-- /#sidebar-wrapper -->

      <!-- Page Content -->
      <div id="page-content-wrapper">
          <div class="container-fluid">
              <br>
              <br>
              <br>
              <?php
                   include 'isi.php';
                   ?>
          </div>
      </div>
      <!-- /#page-content-wrapper -->
  </div>

    <!-- Optional JavaScript -->
    <script src="/bootstrap/js/jquery.min.js" ></script>
    <script src="/bootstrap/js/popper.js" ></script>
    <script src="/bootstrap/js/bootstrap.js" ></script>
  </body>
</html>